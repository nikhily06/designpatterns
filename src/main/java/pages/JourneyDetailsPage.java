package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class JourneyDetailsPage {

    WebDriver driver;

    @FindBy(id = "onetrust-accept-btn-handler")
    WebElement acceptCookiesButton;

    @FindBy(id = "from.search")
    WebElement fromInput;

    @FindBy(id = "to.search")
    WebElement toInput;

    @FindBy(id = "return")
    WebElement returnRadioButton;

    @FindBy(xpath = "//button[contains(@class, '_vj576kNaN')]//span[text() = 'Tomorrow']")
    WebElement tomorrowButton;

    @FindBy(xpath = "//button[contains(@class, '_vj576kNaN')]//span[text() = 'Next day']")
    WebElement nextDayButton;

    @FindBy(xpath = "//button[@class='_nsi73u8']")
    WebElement getTicketsButton;

    public JourneyDetailsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickAcceptCookies() {
        acceptCookiesButton.click();
    }

    public void enterFromLocation(String fromLocation) {
        fromInput.sendKeys(fromLocation + Keys.RETURN);
    }

    public void enterToLocation(String toLocation) {
        toInput.sendKeys(toLocation + Keys.RETURN);
    }

    public void selectReturnTypeJourney() {
        returnRadioButton.click();
    }

    public void clickTomorrow() {
        tomorrowButton.click();
    }

    public void clickNextDay() {
        nextDayButton.click();
    }

    public void clickGetTicketsButton() {
        getTicketsButton.click();
    }

}
