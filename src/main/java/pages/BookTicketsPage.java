package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BookTicketsPage {

    WebDriver driver;

    @FindBy(xpath = "//button[@class = '_1rrhiqra']")
    WebElement continueButton;

    public BookTicketsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickContinue() {
        continueButton.click();
    }

}
