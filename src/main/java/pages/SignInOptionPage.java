package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInOptionPage {

    WebDriver driver;

    @FindBy(xpath = "//input[@name='isGuest'][@value='true']")
    WebElement newCustomerRadioButton;

    @FindBy(xpath = "//input[@name='email']")
    WebElement emailInput;

    @FindBy(xpath = "//button[@class='jsx-61312879']")
    WebElement continueButton;

    @FindBy(xpath = "//button[@class='_1rrhiqra']")
    WebElement continueToCheckoutButton;

    public SignInOptionPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickNewCustomerButton() {
        newCustomerRadioButton.click();
    }

    public void enterEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void clickContinue() {
        continueButton.click();
    }

    public void clickCheckoutButton() {
        continueToCheckoutButton.click();
    }

}
