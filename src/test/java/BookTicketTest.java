import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.BookTicketsPage;
import pages.JourneyDetailsPage;
import pages.SignInOptionPage;

import java.util.concurrent.TimeUnit;

public class BookTicketTest {

    public static WebDriver driver;

    JourneyDetailsPage journeyDetailsPage;
    BookTicketsPage bookTicketsPage;
    SignInOptionPage signInOptionPage;

    @BeforeTest
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.thetrainline.com/");
    }

    @Test (priority = 1)
    public void enterJourneyDetails() {
        journeyDetailsPage = new JourneyDetailsPage(driver);
        journeyDetailsPage.clickAcceptCookies();
        journeyDetailsPage.enterFromLocation("London");
        journeyDetailsPage.enterToLocation("Birmingham");
        journeyDetailsPage.selectReturnTypeJourney();
        journeyDetailsPage.clickTomorrow();
        journeyDetailsPage.clickNextDay();
        journeyDetailsPage.clickGetTicketsButton();
    }

    @Test (priority = 2)
    public void bookTickets() {
        bookTicketsPage = new BookTicketsPage(driver);
        bookTicketsPage.clickContinue();
    }

    @Test (priority = 3)
    public void signInAndConfirm() {
        signInOptionPage = new SignInOptionPage(driver);
        signInOptionPage.clickNewCustomerButton();
        signInOptionPage.enterEmail("abc@xyz.com");
        signInOptionPage.clickContinue();
        signInOptionPage.clickCheckoutButton();
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

}
